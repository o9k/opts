resource "digitalocean_vpc" "o9k" {
  name     = "o9k"
  region   = "fra1"
  ip_range = "10.0.0.0/16"
}
