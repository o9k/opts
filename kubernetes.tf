resource "digitalocean_kubernetes_cluster" "o9k" {
  depends_on = [digitalocean_vpc.o9k]

  name    = "o9k"
  region  = "fra1"
  version = "1.20.2-do.0"

  vpc_uuid = digitalocean_vpc.o9k.id

  node_pool {
    name       = "o9k"
    size       = "s-1vcpu-2gb"
    auto_scale = true
    min_nodes  = 2
    max_nodes  = 4
  }
}

resource "kubernetes_secret" "o9kio" {
  metadata {
    name = "o9kio"
  }

  data = {
    "tls.crt" = file("${path.module}/certs/o9kio.crt")
    "tls.key" = file("${path.module}/certs/o9kio.key")
  }

  type = "kubernetes.io/tls"
}

resource "helm_release" "nginx" {
  name       = "nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"

  set {
    name  = "controller.extraArgs.default-ssl-certificate"
    value = "default/o9kio"
  }

  set {
    name  = "controller.extraArgs.enable-ssl-passthrough"
    value = ""
  }
}
