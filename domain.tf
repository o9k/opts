resource "digitalocean_domain" "o9k" {
  name = "o9k.io"
}

resource "digitalocean_record" "a" {
  domain = digitalocean_domain.o9k.name

  type  = "A"
  name  = "@"
  value = "67.207.75.157"
}

resource "digitalocean_record" "auth" {
  domain = digitalocean_domain.o9k.name

  type  = "A"
  name  = "auth"
  value = "67.207.75.157"
}

resource "digitalocean_record" "failsafe" {
  domain = digitalocean_domain.o9k.name

  type  = "A"
  name  = "failsafe"
  value = "67.207.75.157"
}

resource "digitalocean_record" "cname" {
  domain = digitalocean_domain.o9k.name

  type  = "CNAME"
  name  = "www"
  value = "o9k.io."
}
