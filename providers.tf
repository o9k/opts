terraform {
  backend "remote" {
    organization = "o9k"

    workspaces {
      name = "opts"
    }
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.5.1"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.0.2"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }
  }
}

provider "digitalocean" {
  token = var.digitalocean_token
}

provider "kubernetes" {
  host  = digitalocean_kubernetes_cluster.o9k.endpoint
  token = digitalocean_kubernetes_cluster.o9k.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.o9k.kube_config[0].cluster_ca_certificate
  )
}

provider "helm" {
  kubernetes {
    host  = digitalocean_kubernetes_cluster.o9k.endpoint
    token = digitalocean_kubernetes_cluster.o9k.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.o9k.kube_config[0].cluster_ca_certificate
    )
  }
}

